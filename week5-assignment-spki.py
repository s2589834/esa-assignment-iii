#!/bin/env python3
# Found on https://stackoverflow.com/questions/12911373/how-do-i-use-a-x509-certificate-with-pycrypto
# Needs pycrypto
from Crypto.Util.asn1 import DerSequence
from Crypto.PublicKey import RSA
from binascii import a2b_base64

# Convert from PEM to DER
pem = open("mycert.pem").read()
lines = pem.replace(" ",'').split()
der = a2b_base64(''.join(lines[1:-1]))

# Extract subjectPublicKeyInfo field from X.509 certificate (see RFC3280)
cert = DerSequence()
cert.decode(der)
tbsCertificate = DerSequence()
tbsCertificate.decode(cert[0])
subjectPublicKeyInfo = tbsCertificate[6]
cat t.txt | rev | cut -d '.' -f2 | rev | sort | uniq -c | awk '$1 >= 100 {print$2}' | xargs
