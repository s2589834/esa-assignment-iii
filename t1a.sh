#!/bin/bash

if [ $# != 2 ]
then
    echo "./t1a.sh avrofile1 avrofile2"
fi

# Gather the number of total domains per TLD
echo Gather the number of total domains per TLD
avrocat "$1" | jq -r '.query_name' | sort | uniq | rev | cut -d '.' -f2 | rev | sort | uniq -c | awk '$1 >= 100 {print $2,$1}' > "${1##*/}"_TLD_total_domains
avrocat "$2" | jq -r '.query_name' | sort | uniq | rev | cut -d '.' -f2 | rev | sort | uniq -c | awk '$1 >= 100 {print $2,$1}' > "${2##*/}"_TLD_total_domains

cat "${1##*/}"_TLD_total_domains "${2##*/}"_TLD_total_domains | sort > TLD_total_domains


# Get all signed domains
echo Get all signed domains
avrocat "$1" | jq -r 'select(.rrsig_signature != null) | .query_name'| sort | uniq | rev | cut -d '.' -f2 | rev | sort | uniq -c | awk '$1 >= 100 {print $2,$1}' > "${1##*/}"_TLD_signed_domains
avrocat "$2" | jq -r 'select(.rrsig_signature != null) | .query_name'| sort | uniq | rev | cut -d '.' -f2 | rev | sort | uniq -c | awk '$1 >= 100 {print $2,$1}' > "${2##*/}"_TLD_signed_domains

cat "${1##*/}"_TLD_signed_domains "${2##*/}"_TLD_signed_domains | sort > TLD_signed_domains

echo Get all domains
#avrocat "$1" | jq -r '.query_name' | sort > "${1##*/}"_all_domains
#avrocat "$2" | jq -r '.query_name' | sort > "${2##*/}"_all_domains

echo join all domains with the signed ones
# join "${1##*/}"_all_domains "${1##*/}"_signed_domains > "${1##*/}"_all_signed_domains
# join "${2##*/}"_all_domains "${2##*/}"_signed_domains > "${2##*/}"_all_signed_domains

# cat "${1##*/}"_all_signed_domains "${2##*/}"_all_signed_domains > all_signed_domains

# echo "reduce to only the TLD" 
# rm TLD_total_signed_domains_u
# touch TLD_total_signed_thedomains_u
# while read -r key value; do
#     echo "$(echo $key | rev | cut -d '.' -f2 | rev) $value" >> TLD_total_signed_domains_u
# done < all_signed_domains

# awk '{ seen[$1] += $2 } END { for (i in seen) print i, seen[i] }' TLD_total_signed_domains_u | sort > TLD_total_domains_signed
# rm TLD_total_signed_domains_u
join TLD_total_domains TLD_signed_domains |  awk 'OFS="," {print $1,($3/$2*100) }' | sort -g -r -t ',' -k 2 > TLD_signed_perc.csv


